# Personal .dotfiles

A collection of personal dotfiles and scripts for my Arch install. Please feel free to borrow from it as you see fit.

Scripts designed for use with T2 Kernel on an Intel MacBook Pro.

## Dependencies

```
paru -S hyprland
```

## Setup
```sh
git init --bare $HOME/.git/dotfiles
echo "alias gitdots='git --git-dir=$HOME/.git/dotfiles --work-tree=$HOME'" >> $HOME/.zshrc
gitdots remote add origin gitlab@gitlab.com:matt.kavanagh/dotfiles.git
```

## Replication
```sh
git clone --separate-git-dir=$HOME/.git/dotfiles https://gitlab.com/matt.kavanagh/dotfiles.git $HOME/.dotfiles-tmp
rsync --recursive --verbose --exclude '.git' $HOME/.dotfiles-tmp/ $HOME/
rm --recursive $HOME/.dotfiles-tmp
```

## Configuration
```sh
gitdots config status.showUntrackedFiles no
gitdots remote set-url origin gitlab@gitlab.com:matt.kavanagh/my-dotfiles.git
```

## Usage
```sh
gitdots status
gitdots add <file | -u>
gitdots commit -m 'Commit Message'
gitdots push
```

