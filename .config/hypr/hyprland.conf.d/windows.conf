general {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    gaps_in = 3
    gaps_out = 6
    border_size = 2
    col.active_border = $mauve $peach 45deg
    col.inactive_border = $base

    layout = dwindle

    # Please see https://wiki.hyprland.org/Configuring/Tearing/ before you turn this on
    allow_tearing = false
}

decoration {
    # See https://wiki.hyprland.org/Configuring/Variables/ for more

    rounding = 8
    
    blur {
        enabled = true
        size = 3
        passes = 3
	    blurls = lockscreen
    }

    drop_shadow = yes
    shadow_range = 10
    shadow_render_power = 3
    shadow_offset = 2, 2
    col.shadow = rgba(11111beb)
}

animations {
    enabled = yes

    # Some default animations, see https://wiki.hyprland.org/Configuring/Animations/ for more

    bezier = myBezier, 0.1, 0.9, 0.05, 1.05
	bezier = cubic, 0.645, 0.045, 0.355, 1

    animation = windows, 1, 7, myBezier
    animation = windowsOut, 1, 7, default, popin 80%
    animation = border, 1, 10, default
    animation = borderangle, 1, 30, cubic, loop
    animation = fade, 1, 7, default
    animation = workspaces, 1, 10, default
}

dwindle {
    # See https://wiki.hyprland.org/Configuring/Dwindle-Layout/ for more
    pseudotile = yes # master switch for pseudotiling. Enabling is bound to mainMod + P in the keybinds section below
    preserve_split = yes # you probably want this
}

master {
    # See https://wiki.hyprland.org/Configuring/Master-Layout/ for more
	new_status = master
}

source = ~/.config/hypr/hyprland.conf.d/windowrules.conf
