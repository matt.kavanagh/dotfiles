#!/bin/bash

#Display usage info
usage() {
    echo "Usage: $0 [--random]"
    exit 1
}

# Parse command-line arguments
while [[ "$1" != "" ]]; do
    case $1 in
        --random)
            RANDOM_FLAG=true
            ;;
        *)
            usage
            ;;
    esac
    shift
done

# Check if InputValidation.h exists and remove it if it does
if [ -f InputValidation.h ]; then
    echo "File 'InputValidation.h' already exists. Delete it? (y/n) "
    while true; do
      read confirm
      if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ] && [ "$confirm" != "n" ] && [ "$confirm" != "N" ]; then
        echo "Invalid choice. Please try again: "
        continue
      fi
      if [ "$confirm" == "y" ] || [ "$confirm" == "Y" ]; then
        echo "Removing existing InputValidation.h"
        rm InputValidation.h
        cat ~/Templates/cpp/InputValidation.h > InputValidation.h
        break
      else
        echo "Skipping copy input validation headers."
        break
      fi
    done
else
    cat ~/Templates/cpp/InputValidation.h > InputValidation.h
fi

# Check if main.cpp exists and remove it if it does
if [ -f main.cpp ]; then
    echo "File 'main.cpp' already exists. Delete it? (y/n) "
    while true; do
      read confirm
      if [ "$confirm" != "y" ] && [ "$confirm" != "Y" ] && [ "$confirm" != "n" ] && [ "$confirm" != "N" ]; then
        echo "Invalid choice. Please try again: "
        continue
      fi
      if [ "$confirm" == "y" ] || [ "$confirm" == "Y" ]; then
        echo "Removing existing main.cpp"
        rm main.cpp

        if [ "$RANDOM_FLAG" = true ]; then
          echo '#include "Random.h"' > main.cpp
        fi

        cat ~/Templates/cpp/main.cpp >> main.cpp
        break
      else
        echo "Skipping copy main cpp file."
        break
      fi
    done
else
    cat ~/Templates/cpp/main.cpp >> main.cpp
fi

if [ "$RANDOM_FLAG" = true ]; then
  if [ -f Random.h ]; then
    echo "Removing existing Random.h"
    rm Random.h
  fi
  cat ~/Templates/cpp/Random.h > Random.h
fi


echo "C++ project successfully set up!"
echo "File list:"
ls

exit 0;
