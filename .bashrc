#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

alias ls='ls --color=auto'
alias grep='grep --color=auto'
PS1='[\u@\h \W]\$ '

alias ssh-jellyfin='ssh -p 38128 $USER@192.168.1.5'
alias ssh-kimsufi='ssh -p 56299 $USER@$USER.us'
alias gitdots='/usr/bin/git --git-dir=$HOME/git/dotfiles --work-tree=$HOME'

# eval "$(starship init bash)"
eval "$(zoxide init --cmd cd bash)"

export FZF_DEFAULT_OPTS=" \
--color=bg+:#313244,bg:#1e1e2e,spinner:#f5e0dc,hl:#f38ba8 \
--color=fg:#cdd6f4,header:#f38ba8,info:#cba6f7,pointer:#f5e0dc \
--color=marker:#f5e0dc,fg+:#cdd6f4,prompt:#cba6f7,hl+:#f38ba8"
